﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Quartz.NET.Web.Utility
{
    public static class HttpManager
    {

        public static string GetUserIP(IHttpContextAccessor httpContextAccessor)
        {
            var Request = httpContextAccessor.HttpContext.Request;
            string realIP = string.Empty;
            string forwarded = string.Empty;
            string remoteIpAddress = httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            if (Request.Headers.ContainsKey("X-Real-IP"))
            {
                realIP = Request.Headers["X-Real-IP"].ToString();
                if (realIP != remoteIpAddress)
                {
                    remoteIpAddress = realIP;
                }
            }
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
            {
                forwarded = Request.Headers["X-Forwarded-For"].ToString();
                if (forwarded != remoteIpAddress)
                {
                    remoteIpAddress = forwarded;
                }
            }
            return remoteIpAddress;
        }

        /// <summary>
        /// 2020年11月16日 请求方式改为WebClient
        /// </summary>
        /// <param name="httpClientFactory"></param>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        public static async Task<string> HttpSendAsync(this IHttpClientFactory httpClientFactory,
            HttpMethod method,
            string url,
            Dictionary<string, string> headers = null)
        {
            try
            {
                if (method == HttpMethod.Get)
                {
                    using (var client = new WebClient())
                    {
                        if (headers != null)
                        {
                            foreach (var header in headers)
                            {
                                client.Headers.Add(header.Key, header.Value);
                            }
                        }

                        client.Encoding = Encoding.UTF8;

                        return await client.DownloadStringTaskAsync(url);
                    }
                }
                else
                {
                    var pUrl = url.Split('?');

                    using (var client = new WebClient())
                    {
                        if (headers != null)
                        {
                            foreach (var header in headers)
                            {
                                client.Headers.Add(header.Key, header.Value);
                            }
                        }
                        client.Encoding = Encoding.UTF8;
                        client.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");

                        string postData = pUrl[1];
                        byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                        var response = client.UploadData(pUrl[0], byteArray);
                        return Encoding.UTF8.GetString(response);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return ex.Message;
            }
        }
    }
}
